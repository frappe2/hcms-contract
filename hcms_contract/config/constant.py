import enum

class ContractApprovalWorkflowState(enum.Enum):
	Pending = "Pending"
	Approved = "Approved"
	Rejected = "Rejected"

class ContractStatusMetadata(enum.Enum):
    New = "New"
    Running = "Running"
    Expired = "Expired"
    Cancelled = "Cancelled"

class ContractTypeMetadata(enum.Enum):
    Contract = "Contract"
    Decision = "Decision"
    Appendix = "Appendix"
    Agreement = "Agreement"


class ContractTermMetadata(enum.Enum):
    Internship = "Internship"
    Probation = "Probation"
    FixedTerm = "Fixed-term"
    Permanent = "Permanent"


class ContractWorkflowState(enum.Enum):
    Draft = 'Draft'
    Pending = 'Pending'
    Approved = 'Approved'
    Rejected = 'Rejected'


class ContractRemindDuration(enum.Enum):
	Contract = 30
	Probation = 15


class WorkflowActionsMetadata(enum.Enum):
	RequestForApproval = "Request for approval"
	Approve = "Approve"
	Reject = "Reject"

class OfferLetterTemplate(enum.Enum):
    MAS = "Masterise Services_Offer Letter_Template to IT"
    ExpatMAG = "Masterise Group_Offer Letter_Expat_To IT"
    MAG = "Masterise Group_Offer letter_Template to IT"
    MAP = "Masterise Property_Offer Letter_Template to IT"


class CompanyCode(enum.Enum):
    MAS = "MAS"
    MAP = "MAP"


class LanguageCode(enum.Enum):
    VN = 'vi'
    EN = 'en'
