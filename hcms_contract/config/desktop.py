from frappe import _

def get_data():
	return [
		{
			"module_name": "Hcms Contract",
			"type": "module",
			"label": _("Hcms Contract")
		}
	]
