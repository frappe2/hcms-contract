// Copyright (c) 2022, mas and contributors
// For license information, please see license.txt

// frappe.require([
// 	"/assets/hcms_contract/node_modules/@bundled-es-modules/pdfjs-dist/build/pdf.js",
// 	// "/assets/hcms_contract/node_modules/@bundled-es-modules/pdfjs-dist/build/pdf.worker.js"
// ])

// import pdfLib from "/assets/hcms_contract/node_modules/@bundled-es-modules/pdfjs-dist/build/pdf"

// pdfLib.GlobalWorkerOptions.workerSrc = "/assets/hcms_contract/node_modules/@bundled-es-modules/pdfjs-dist/build/pdf.worker.js";
// import {getDocument} from "/assets/hcms_contract/node_modules/@bundled-es-modules/pdfjs-dist/build/pdf"


const MAX_DURATION = 36

const PersonalDocumentTypeMetadata = {
	CCCD: "Citizen Identity Card/CCCD",
	CMND: "Identity Card/CMND",
	Passport: "Passport",
	DrivingLicense: "Driving License",
	Visa: "Visa",
	WorkPermit: "Work Permit",
	Other: "Other"
}

const ContractStatusMetadata = {
	New: "New",
	Running: "Running",
	Expired: "Expired",
	Cancelled: "Cancelled"
}

const ContractTypeMetadata = {
	Contract: "Contract",
	Decision: "Decision",
	Appendix: "Appendix",
	Agreement: "Agreement"
}

const ContractTermMetadata = {
	Internship: "Internship",
	Probation: "Probation",
	FixedTerm: "Fixed-term",
	Permanent: "Permanent"
}

const ContractApprovalStatus = {
	Pending: "Pending",
	Approved: "Approved",
	Rejected: "Rejected"
}

const WorkflowActionsMetadata = {
	RequestForApproval: "Request for approval",
	Approve: "Approve",
	Reject: "Reject"
}

const Messages = {
	Warning: "Warning",
	SaveFormFirst: 'Please save the form before taking any actions.',
	NotEnoughInfoProbation: "This employee does not have enough information to create a probation contract.",
	RequestForContractApproval: "Request for Contract Approval",
	ApproveContract: "Contract Approval",
	RejectContract: "Contract Rejection",
	CannotUndoneAction: 'This action cannot be undone',
	UnsavedTakeAction: 'Are you sure to save and ',
	SavedTakeAction: 'Are you sure to ',
	WaitingForApproval: 'Please wait for HR directors to approve probation contract before sending to candidate.'
}

const APIPath = {
	HandleWorkflowAction: "hcms_contract.hcms_contract.doctype.contract.contract_services.handle_workflow_action",
	InitHRResponsible: "hcms_contract.hcms_contract.doctype.contract.contract_services.init_hr_responsibility",
}

const DELAY_IN_MILLIS = 1500

var today_date;

var list_actions = []
var current_intro_message = null


/// DIALOGS ///

function show_error_dialog(frm, message) {
	frappe.show_alert({
		message: message,
		indicator: 'red'
	}, 5)
}

function show_warning_dialog(frm, title, message, callback) {
	frappe.warn(
		title,
		message,
		() => {
			callback()
		},
		'Continue',
		true
	)
}

function show_process_dialog() {
	frappe.msgprint({
		title: 'Processing',
		indicator: 'blue',
		message: __('Please wait...')
	})
}

function show_information_dialog(title, message) {
	frappe.msgprint({
		title: title,
		indicator: 'red',
		message: message
	})
}

function show_intro(frm, message, color) {
	if (current_intro_message !== message) {
		current_intro_message = message
		frm.set_intro(message, color)
	}
}


function close_dialog() {
	setTimeout(function () {
		cur_dialog.hide()
	}, DELAY_IN_MILLIS)
}

/// HANDLE WORKFLOW ACTIONS ///

function handle_workflow_action(frm, action, callback_fn) {
	frappe.call({
		method: APIPath.HandleWorkflowAction,
		args: {
			contract: frm.doc.name,
			action: action,
		},
		callback: async function (r) {
			console.log('handle_workflow_action')
			console.log(r.message)
			if (r.message) {
				if (callback_fn !== null && callback_fn !== undefined) {
					callback_fn()
				}
				console.log('reload doc')
				frm.reload_doc()
			}

		}
	})
}


function renderPdfPage(idElementCanvas, linkPdf, zoom = 1, currentPage = 1) {
	getDocument(linkPdf).promise.then(data => {
		data
			.getPage(currentPage)
			.then((page) => {
				console.log('page', page)

				const canvas = document.querySelector(idElementCanvas)
				const ctx = canvas.getContext('2d')
				const viewport = page.getViewport({
					scale: zoom,
				})

				canvas.height = viewport.height
				canvas.width = viewport.width

				// Render the PDF page into the canvas context.
				const renderCtx = {
					canvasContext: ctx,
					viewport: viewport,
				};

				page.render(renderCtx)

				pageNum.textContent = currentPage
			})
	})
}


// function change_workflow_state(frm, new_state, callback_fn){
//     frappe.call({
//         method: "hcms_contract.hcms_contract.doctype.contract.contract.change_workflow_state",
//         args: {
//             contract: frm.doc.name,
//             new_state: new_state,
//         },
//         callback: async function(_){
//             console.log(`state changed to ${new_state}`)
// 			if (callback_fn !== null && callback_fn !== undefined){
// 				await callback_fn()
// 			}
//             frm.reload_doc()
//         }
//     })
// }

/// CHECK CONDITIONS OF CREATING PROBATION CONTRACT ///
function has_personal_id(emp) {
	let personal_documents = emp.personal_doc
	console.log('personal_documents')
	console.log(personal_documents)
	if (personal_documents) {
		for (let i = 0; i < personal_documents.length; i++) {
			if ([PersonalDocumentTypeMetadata.CCCD, PersonalDocumentTypeMetadata.CMND, PersonalDocumentTypeMetadata.Passport].includes(personal_documents[i].type)) {
				if (personal_documents[i].number && personal_documents[i].attached_file) {
					return true
				}
			}
		}
	}
	return false
}

function has_work_permit(emp) {
	let personal_documents = emp.personal_doc
	let has_valid_visa = false
	let has_valid_work_permit = false
	if (personal_documents) {
		for (let i = 0; i < personal_documents.length; i++) {
			if (personal_documents[i].type == PersonalDocumentTypeMetadata.Visa) {
				if (personal_documents[i].expiration_date > frappe.datetime.get_today() && personal_documents[i].attached_file) {
					has_valid_visa = true
				}
			} else if (personal_documents[i].type == PersonalDocumentTypeMetadata.WorkPermit) {
				if (personal_documents[i].expiration_date > frappe.datetime.get_today() && personal_documents[i].attached_file) {
					has_valid_work_permit = true
				}
			}
		}
	}
	return (has_valid_visa && has_valid_work_permit)
}

function has_permanent_address(emp) {
	return (emp.permanent_address_province && emp.permanent_address_district && emp.permanent_address_ward && emp.permanent_address_street)
}

function has_current_address(emp) {
	return (emp.current_address_province && emp.current_address_district && emp.current_address_ward && emp.current_address_street)
}

async function can_create_job_offer(frm) {
	if (frm.doc.employee && frm.doc.term == ContractTermMetadata.Probation) {
		let emp = await Promise.resolve(frappe.db.get_doc('Employee', frm.doc.employee))
		console.log('emp')
		console.log(emp)
		// if (has_permanent_address(emp) && has_current_address(emp) && ((emp.nationality == "Vietnam" && has_personal_id(emp)) || has_work_permit(emp))){
		if ((emp.nationality == "Vietnam" && has_personal_id(emp)) || has_work_permit(emp)) {
			return true
		}
		return false
	}
	return null
}


function display_is_primary_contract(frm, should_update_is_primary_contract = false) {
	let is_contract = frm.doc.type == ContractTypeMetadata.Contract
	frm.toggle_display(['is_primary'], is_contract)
	if (!is_contract && should_update_is_primary_contract) {
		frm.set_value('is_primary', null)
	}
}

function get_contract_status(frm) {
	if (frm.doc.enable) {
		let start_date = frm.doc.start_date
		let end_date = frm.doc.end_date
		let contract_term = frm.doc.term

		if (start_date) {
			if (end_date) {
				console.log(today_date)
				console.log(start_date)
				if (today_date < start_date) {
					return ContractStatusMetadata.New
				} else if ((start_date <= today_date) && (today_date <= end_date)) {
					return ContractStatusMetadata.Running
				} else if (end_date < today_date) {
					return ContractStatusMetadata.Expired
				}
			} else {
				if (contract_term == ContractTermMetadata.Permanent) {
					if (today_date < start_date) {
						return ContractStatusMetadata.New
					} else if (start_date <= today_date) {
						return ContractStatusMetadata.Running
					}
				} else {
					return null
				}
			}
		} else {
			return null
		}
	} else {
		return ContractStatusMetadata.Cancelled
	}

}

function init_duration(frm, should_change_duration_value = false) {
	if (frm.doc.term == ContractTermMetadata.FixedTerm) {
		frm.toggle_display(['duration'], true)
		set_options_for_duration(frm, MAX_DURATION)
	} else {
		if (should_change_duration_value) {
			frm.set_value('duration', null)
		}
		frm.toggle_display(['duration'], false)
	}

}

function check_if_primary_contract_overlap(frm) {
	frappe.db.get_list(
		hcms_core.constants.DoctypeName.Contract,
		{
			fields: ['name', 'start_date', 'end_date'],
			filters: {
				employee: frm.doc.employee,
				is_primary: true
			}

		}
	).then(records => {
		console.log('contract code generated')
		console.log(frm.doc.name)
		if (records.length == 1 && records[0].name != frm.doc.name) {
			let prev_primary_contract = records[0]
			let current_start_date = frm.doc.start_date
			let current_end_date = frm.doc.end_date
			let prev_start_date = prev_primary_contract.start_date
			let prev_end_date = prev_primary_contract.end_date

			if (!(current_end_date < prev_start_date || prev_end_date < current_start_date)) {
				frappe.throw('Overlap Primary Contract')
			} else {
				frm.save()
			}
		}

	})
}

function set_hr_responsible_query(frm) {
	frm.set_query('hr_responsible', () => {
		return {
			filters: {
				department: "HRAD"
			}
		}
	})
}

async function init_hr_responsible(frm) {
	set_hr_responsible_query(frm)

	console.log('init_hr_responsible')
	console.log(frm.doc.hr_responsible)
	if (frm.doc.hr_responsible === undefined) {
		let init_hr = await Promise.resolve(
			frappe.call({
				method: APIPath.InitHRResponsible,
			})
		)
		console.log('init_hr')
		console.log(init_hr)
		if (init_hr && init_hr.message) {
			frm.set_value('hr_responsible', init_hr.message)
		}
	}



}

function set_options_for_duration(frm, max_duration) {
	let list_duration_options = []
	for (let i = 1; i <= max_duration; i++) {
		list_duration_options.push(i.toString())
	}
	frm.set_df_property('duration', 'options', list_duration_options)
}


/// WORKFLOW ACTION BUTTONS ///

function get_list_actions(frm, workflow) {
	let list_actions = []

	if (workflow && workflow.transitions) {

		workflow.transitions.forEach(transition => {
			if (frappe.user.has_role(transition.allowed) && (frm.doc.workflow_state == transition.state)) {
				list_actions.push({
					'action': transition.action,
					'next_state': transition.next_state
				})
			}
		})
	}

	return list_actions
}

function get_contract_workflow(frm) {
	return frappe.db.get_doc(hcms_core.constants.DoctypeName.Workflow, 'Contract Workflow')
}

function create_list_action_buttons(frm) {
	list_actions.forEach(action => {

		switch (action.action) {
			case WorkflowActionsMetadata.RequestForApproval:
				create_workflow_button(frm, action)
				break

			case WorkflowActionsMetadata.Reject:
				create_workflow_button(frm, action)
				break

			case WorkflowActionsMetadata.Approve:
				create_workflow_button(frm, action)
				break
		}
	})
}

function display_intro_message(frm) {
	switch (frm.doc.workflow_state) {
		case ContractApprovalStatus.Pending:
			if (frappe.user.has_role("Talent Acquisition Manager")) {
				show_intro(frm, __(Messages.WaitingForApproval), 'orange')
			}
			break
	}
}

function get_dialog_workflow_message(frm, core_message) {
	if (frm.is_dirty()) {
		return __(Messages.UnsavedTakeAction).concat(core_message)
	}
	return __(Messages.SavedTakeAction).concat(core_message)
}


function set_primary_fields_for_contract(frm) {

	let is_required = frm.doc.type === ContractTypeMetadata.Contract

	frm.set_df_property('wage', 'reqd', is_required)
	frm.set_df_property('department', 'reqd', is_required)
	frm.set_df_property('job_position', 'reqd', is_required)
	frm.set_df_property('position_band', 'reqd', is_required)


}

function view_and_download_files(frm) {

	/// Download Docx file
	if (frm.doc.docx_file !== undefined && frm.doc.docx_file !== null) {
		frm.add_custom_button(__("Docx"), function () {
			let link = document.createElement('a')
			link.href = frm.doc.docx_file
			link.download = ''
			link.click()
		}, __("Download"))
	}


	if (frm.doc.pdf_file !== undefined && frm.doc.pdf_file !== null) {
		/// Download PDF file
		frm.add_custom_button(__("Pdf"), function () {
			let link = document.createElement('a')
			link.href = frm.doc.pdf_file
			link.download = ''
			link.click()
		}, __("Download"))

		/// View
		frm.add_custom_button(__("View"), function () {
			// let d = new frappe.ui.Dialog({
			// 	title: `${frm.doc.name} - ${frm.doc.employee_name}`,
			// 	fields: [
			// 		{
			// 			fieldname: 'pdf_view',
			// 			fieldtype: 'HTML'
			// 		}
			// 	]
			// });

			// d.set_value('pdf_view', '<canvas id="dialog_pdf_viewer" width="200" height="100"></canvas>')
			// d.show()
			// renderPdfPage('dialog_pdf_viewer', frm.doc.pdf_file)

			// var contractTab = window.open(frm.doc.pdf_file)

			console.log('pdf file url')

			let d = new frappe.ui.Dialog({
				title: `${frm.doc.name} - ${frm.doc.employee_name}`,
				fields: [
					{
						fieldname: 'pdf_view',
						fieldtype: 'HTML'
					}
				]
			});

			/// time is added as a param to always load new file
			// TODO: remove replace
			d.set_value('pdf_view', `<object data="${frm.doc.pdf_file}" width="100%" height="700"></object>`)
			d.show()
			$(d.$wrapper.find('.modal-dialog')).addClass('modal-lg')
		})
	}
}


// async function execute(frm, callback){
//     if (frm.is_dirty()){
//         await Promise.resolve(frm.save())
//     }
//     callback()
// }

function create_workflow_button(frm, action) {
	frm.add_custom_button(__(action.action), async () => {
		let title = ''
		let message = ''
		console.log('next state')
		console.log(action.next_state)
		let callback_fn = async () => {
			await hcms_core.utils.execute(frm, () => {
				show_process_dialog()
				handle_workflow_action(frm, action.action, () => {
					console.log('hide process dialog')
					close_dialog()
				})
			})
		}
		switch (action.action) {
			case WorkflowActionsMetadata.RequestForApproval:
				title = __(Messages.RequestForContractApproval)
				message = get_dialog_workflow_message(frm, `request HR directors for ${frm.doc.employee_name}'s contract approval?`)
				show_warning_dialog(frm, title, message, callback_fn)
				break

			case WorkflowActionsMetadata.Approve:
				title = __(Messages.ApproveContract)
				message = get_dialog_workflow_message(frm, `approve this contract?`)
				show_warning_dialog(frm, title, message, callback_fn)
				break

			case WorkflowActionsMetadata.Reject:
				callback_fn = () => {
					hcms_core.utils.execute(frm, () => {
						show_process_dialog()
						handle_workflow_action(frm, action.action, () => {
							console.log('hide process dialog')
							cur_dialog.hide()
						})
					})
				}

				title = 'Contract Rejection'
				message = get_dialog_workflow_message(frm, `reject ${frm.doc.employee_name}'s contract?`)
				show_warning_dialog(frm, title, message, callback_fn)
				break

		}
	})
}

function set_year_code(frm) {
	if (!frm.doc.year_code) {
		let year_code = frappe.datetime.get_today().substring(2, 4)
		console.log('year_code')
		console.log(year_code)
		frm.set_value('year_code', year_code)
	}
}


function init_contract_type_related_fields(frm, should_set_value = false) {
	let current_contract_type = frm.doc.type
	if (current_contract_type) {
		if (current_contract_type == ContractTypeMetadata.Contract) {
			/// display contract term when contract type is Contract
			frm.toggle_display(['term'], true)

			/// disable and hide parent contact
			if (should_set_value) {
				frm.set_value('amend_to', null)
			}
			frm.toggle_display(['amend_to'], false)
		} else {
			if (should_set_value) {
				frm.set_value('term', null)
			}
			frm.toggle_display(['term'], false)

			/// show parent contract, only show contract whose contract type is Contract
			frm.toggle_display(['amend_to'], true)
			frm.set_query('amend_to', () => {
				return {
					filters: {
						type: ContractTypeMetadata.Contract
					}
				}
			})
		}
	} else {
		/// diable and hide contract term and parent contract
		if (should_set_value) {
			frm.set_value('term', null)
			frm.set_value('amend_to', null)
		}
		frm.toggle_display(['term'], false)
		frm.toggle_display(['amend_to'], false)
	}
}

frappe.ui.form.on('Contract', {

	refresh: async function (frm) {
		get_contract_workflow(frm).then(r => {
			list_actions = get_list_actions(frm, r)
			create_list_action_buttons(frm)
			display_intro_message(frm)
		})

		view_and_download_files(frm)


	},

	onload: async function (frm) {
		current_intro_message = null
		today_date = frappe.datetime.get_today()

		// set_options_for_duration(frm, max_duration)
		init_duration(frm)
		init_contract_type_related_fields(frm)
		display_is_primary_contract(frm)
		await init_hr_responsible(frm)

		set_year_code(frm)
	},

	employee: async function (frm) {
		let allow_job_offer = await can_create_job_offer(frm)
		if (allow_job_offer === false) {
			frappe.msgprint({
				title: __(Messages.Warning),
				indicator: 'red',
				message: __(Messages.NotEnoughInfoProbation)
			})
			frm.set_value('employee', null)
			frm.set_value('employee_name', null)
		}

	},


	/// show contract term when contract type = contract
	/// show parent contract when contract type != contract
	type: function (frm) {
		display_is_primary_contract(frm, true)
		init_contract_type_related_fields(frm, true)
		set_primary_fields_for_contract(frm)
	},

	term: async function (frm) {
		let allow_job_offer = await can_create_job_offer(frm)
		if (allow_job_offer === false) {
			frappe.msgprint({
				title: __(Messages.Warning),
				indicator: 'red',
				message: __(Messages.NotEnoughInfoProbation)
			})
			frm.set_value('term', null)
		} else {
			/// end date: required except when contract term = permanent
			frm.toggle_reqd('end_date', frm.doc.term != 'Permanent');

			/// duration: only shown when contract term is Fixed-term
			init_duration(frm, true)

			frm.set_value('status', get_contract_status(frm))
		}
	},

	start_date: function (frm) {

		let start_date = frm.doc.start_date
		let end_date = frm.doc.end_date

		if (start_date && end_date) {
			if (start_date > end_date) {
				frm.set_value('status', null)
				frm.set_value('start_date', null)
				frappe.throw('Start Date must be less than or equal to End Date')
			}
		}

		frm.set_value('status', get_contract_status(frm))

	},

	end_date: function (frm) {

		let start_date = frm.doc.start_date
		let end_date = frm.doc.end_date

		if (start_date && end_date) {
			if (start_date > end_date) {
				frm.set_value('end_date', null)
				frm.set_value('status', null)
				frappe.throw('Start Date must be less than or equal to End Date')

			}
		}

		frm.set_value('status', get_contract_status(frm))
	},

	is_primary_contract: function (frm) {
		if (frm.doc.is_primary_contract == 1) {
			check_if_primary_contract_overlap(frm)
		}
	},

	enable: function (frm) {
		frm.set_value('status', get_contract_status(frm))
	},

	approval_status: function (frm) {
		frm.set_value('enable', !(frm.doc.approval_status == ContractApprovalWorkflowState.Rejected))
	}
});
