# Copyright (c) 2022, mas and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.utils import today, getdate
from hcms_contract.config.constant import *
from hcms.config.constant import *
import time

from docxtpl import DocxTemplate
import subprocess
import os
from pathlib import Path
from hcms.human_capital_management_system.doctype.employee.employee import Employee
import traceback
import hcms_core.utils.constants as core


class Contract(Document):

    def get_contract_status(self):
        if (self.enable):
            today_date = getdate()
            start_date = self.start_date
            end_date = self.end_date
            contract_term = self.term

            if (start_date):
                if (end_date):
                    if (today_date < start_date):
                        return ContractStatusMetadata.New
                    elif ((start_date <= today_date) and (today_date <= end_date)):
                        return ContractStatusMetadata.Running
                    elif (end_date < today_date):
                        return ContractStatusMetadata.Expired

                else:
                    if (contract_term == ContractTermMetadata.Permanent):
                        if (today_date < start_date):
                            return ContractStatusMetadata.New
                        elif (start_date <= today_date):
                            return ContractStatusMetadata.Running
                    else:
                        return None
            else:
                return None

        else:
            return ContractStatusMetadata.Cancelled

    def _generate_pdf(self, doc_path, path):
        subprocess.run(['soffice',
                        '--headless',
                        '--convert-to',
                        'pdf',
                        '--outdir',
                        path,
                        doc_path])

        return Path(doc_path).with_suffix('.pdf')

    def _set_function_level_from_position_band(self, position_band):
        if (position_band):
            function_level_res = frappe.db.get_value(
                'Position Band', position_band, 'function_level')
            self.function_level = function_level_res
        else:
            self.function_level = None

    def _sync_to_file_manager(self, file_name, file_url, dt, dn, folder, file_size, is_private=True, df=None, is_force_new=False):
        path = os.path.join("Home", folder)
        print('_sync_to_file_manager path')
        print(path)
        self._validate_folder_file_manager(path)

        def insert_new_doc():
            print('insert_new_doc called')
            f = frappe.get_doc(
                {
                    "doctype": core.CoreConstants.DoctypeName.File,
                    "file_name": file_name,
                    "file_url": file_url,
                    "attached_to_doctype": dt,
                    "attached_to_name": dn,
                    "attached_to_field": df,
                    "folder": path,
                    "file_size": file_size,
                    "is_private": is_private,
                    "is_file_saved": True,
                    "is_ignore_validate": True,
                }
            )
            f.insert()
            print('insert new doc completed')

        def update_doc():
            print('update_doc called')
            try:
                f = frappe.get_last_doc(
                    core.CoreConstants.DoctypeName.File, filters={"file_name": file_name, "folder": path}, for_update=True)
                f.save()
            except:
                insert_new_doc()

        try:
            if is_force_new:
                insert_new_doc()
            else:
                update_doc()
        except Exception:
            print('_sync_to_file_manager failed')
            traceback.print_exc()

    def _validate_folder_file_manager(self, path):
        print('_validate_folder_file_manager path')
        print(path)
        head, tail = os.path.split(path)
        if not frappe.db.exists(core.CoreConstants.DoctypeName.File, path):
            if head != "":
                folder = self._validate_folder_file_manager(head)
                frappe.get_doc({
                    "doctype": core.CoreConstants.DoctypeName.File,
                    "file_name": tail,
                    "is_folder": 1,
                    "folder": folder,
                }).insert(set_name=path)
                return path
            else:
                return "Home"
        return path

    def onload(self):
        print('onload contract')
        self._set_function_level_from_position_band(self.position_band)

    def _format_date_dmy(self, datestr) -> str:
        return datestr.strftime("%d/%m/%Y")

    def _get_personal_id_info(self, employee_doc: Employee):
        personal_documents = employee_doc.personal_doc
        reference_personal_documents = [PersonalDocumentTypeMetadata.CCCD.value, PersonalDocumentTypeMetadata.CMND.value]
        if (personal_documents):
            for i in range(len(personal_documents)):
                if ((personal_documents[i].type in reference_personal_documents) and (personal_documents[i].primary)):
                    return personal_documents[i].number, self._format_date_dmy(personal_documents[i].issued_date), personal_documents[i].issued_by
        return None, None, None

    def _get_full_address(self, employee_doc: Employee) -> str:
        ward = ''
        district = ''
        province = ''
        if (employee_doc.current_address_ward):
            ward = f"{frappe.db.get_value(core.CoreConstants.DoctypeName.Ward, employee_doc.current_address_ward, 'ward')}, "
            print(ward)
        if (employee_doc.current_address_district):
            district = f"{frappe.db.get_value(core.CoreConstants.DoctypeName.District, employee_doc.current_address_district, 'district')}, "

        if (employee_doc.current_address_province):
            province = f"{frappe.db.get_value(core.CoreConstants.DoctypeName.Province, employee_doc.current_address_province, 'province')}, "

        return f"{employee_doc.current_address_street}, {ward}{district}{province}{employee_doc.current_address_country}"

    def __get_job_position_contract(self, job_position: str, language: LanguageCode = LanguageCode.VN) -> str:
        print(f"__get_job_position_contract = {job_position}")
        # title_in_contract = frappe.db.get_value('Job Position', job_position, 'title_in_contract' if language == LanguageCode.VN else 'title_en')
        title_in_contract = frappe.db.get_value(core.CoreConstants.DoctypeName.JobPosition, job_position, 'title_in_contract')
        title_in_contract = frappe._(msg=f'{title_in_contract}', lang=language.value)
        print(
            f"__get_job_position_contract title_in_contract = {title_in_contract}")
        return title_in_contract

    def __get_erc_information(self):
        company_doc = frappe.get_doc(core.CoreConstants.DoctypeName.Company, self.company)
        return company_doc.address, company_doc.erc_business_tax_id, company_doc.erc_date_of_issue, company_doc.erc_issued_by

    def __get_function_level(self, position_band: str) -> str:
        return frappe.db.get_value(core.CoreConstants.DoctypeName.PositionBand, position_band, 'function_level')

    def __get_approver_name_uppercase(self, signed_by: str) -> str:
        return frappe.db.get_value(core.CoreConstants.DoctypeName.BusinessRepresentative, signed_by, 'full_name_display').upper()

    def _get_company_name(self) -> str:
        return frappe.db.get_value(core.CoreConstants.DoctypeName.Company, self.company, 'full_name')

    def _get_department_name(self) -> str:
        return frappe.db.get_value(core.CoreConstants.DoctypeName.Department, self.department, 'department_vn')

    def __get_money_format(self, wage, langCode: LanguageCode = LanguageCode.VN):
        if (wage):
            # wage_unit: str = frappe._(msg='VND/month', lang=str(langCode.value))
            wage_unit: str = frappe._(msg='VND/month', lang=langCode.value)
            print('__get_money_format wage unit')
            print(wage_unit)
            return f"{wage:,} {wage_unit}"

    # Expect the format of date is yyyy-mm-dd, which is the default format

    def __get_date_format(self, date: str = '', lang: LanguageCode = LanguageCode.VN) -> str:
        res = ''

        if (not date):
            return res

        date_arr = date.split('-')
        if (len(date_arr) == 3):
            match (lang):
                case LanguageCode.VN:
                    res = f'Ngày {date_arr[-1].zfill(2)} tháng {date_arr[1].zfill(2)} năm {date_arr[0]}'

        return res

    def generate_temp_public_contract(self,):
        docx_path = f"{os.getcwd()}/{frappe.local.site}/{self.docx_file}"
        temp_public_contract_dir = f"{os.getcwd()}/{frappe.local.site}/public/files/temp/{self.employee}"
        frappe.create_folder(temp_public_contract_dir)

        self._generate_pdf(docx_path, temp_public_contract_dir)
        return temp_public_contract_dir

    def __get_contract_context(self, employee_doc: Employee) -> dict:
        context: dict = {}
        id_number, id_issued_date, id_issued_by = self._get_personal_id_info(employee_doc=employee_doc)
        erc_address, tax_id, erc_date_of_issue, erc_issued_by = self.__get_erc_information()

        context = {
            # new keys
            "company_name": employee_doc.get_company_full_name(),
            "document_number": self.name,
            "document_created_date": self.__get_date_format(today()),
            "employee_name": employee_doc.full_name,
            "date_of_birth": self._format_date_dmy(getdate(employee_doc.date_of_birth)),
            "id_card_number": id_number,
            "id_card_issued_date": id_issued_date,
            "id_card_issued_by": id_issued_by,
            "postage_address": self._get_full_address(employee_doc=employee_doc),
            "erc_tax_id": tax_id,
            "erc_issued_by": erc_issued_by,
            "erc_tax_at": self._format_date_dmy(getdate(erc_date_of_issue)),
            "erc_address": erc_address,
            "title": self.__get_job_position_contract(job_position=self.job_position),
            "position_band": self.position_band,
            "function_level": self.__get_function_level(position_band=self.position_band),
            "department": self.department,
            "line_manager_position": employee_doc.get_report_line_title(),
            "onboarding_date": self._format_date_dmy(getdate(self.start_date)),
            "gross_salary": self.__get_money_format(wage=self.wage,),
            "probation_salary_percent": f"{self.probation_salary_percent}%",
            "document_approver": self.__get_approver_name_uppercase(signed_by=self.signed_by),

            # old keys
            "employee_reportingline_eng": employee_doc.get_report_line_title(langCode=LanguageCode.EN),
            "document_sendingdate": self.__get_date_format(today()),
            "employee_fullname": employee_doc.full_name,
            "date_of_birth": employee_doc.date_of_birth,
            "ID_number": id_number,
            "ID_issuedate": id_issued_date,
            "ID_issuedby": id_issued_by,
            "current_address_vie": self._get_full_address(employee_doc=employee_doc),
            "employee_title_vi": self.__get_job_position_contract(job_position=self.job_position, language=LanguageCode.VN),
            "employee_title_eng": self.__get_job_position_contract(job_position=self.job_position, language=LanguageCode.EN),
            "employee_positionband": self.position_band,
            "employee_functionlevel": self.__get_function_level(position_band=self.position_band),
            "employee_onboardingdate": self._format_date_dmy(getdate(self.start_date)),
            "employee_grosssalary": self.__get_money_format(wage=self.wage),
            "probation_salarypercent": f"{self.probation_salary_percent}%",
            "document_approver_uppercase": self.__get_approver_name_uppercase(signed_by=self.signed_by),
        }

        print(context)
        return context

    def _remove_file(self, filename: str):
        print(f'remove file {filename}')
        if (os.path.exists(filename)):
            os.remove(filename)
            print(f'delete {filename} success')

    def _delete_old_contract(self, parent_folder_path: str):
        # delete from local folder
        list_files = os.listdir(parent_folder_path)
        for filename in list_files:
            if (self.name in filename):
                self._remove_file(filename=f"{parent_folder_path}/{filename}")
                self._remove_file(filename=f"{parent_folder_path}/{filename}")
        # delete from DB
        frappe.db.delete(core.CoreConstants.DoctypeName.File, {
            'file_name': ['like', '{}%'.format(self.name)]
        })

    def _generate_offer_letter(self, contract_code, employee: Employee):
        print('_generate_offer_letter')
        print(f'{self.type}:{self.term}')

        template = frappe.get_last_doc(
            core.CoreConstants.DoctypeName.Template, filters={
                'type': 'Contract',
                'option': f'{self.type}:{self.term}',
                'is_apply_expat': 1 if employee.nationality == frappe.get_system_settings('country') else 0
            })

        print(template)

        doc = DocxTemplate(frappe.get_site_path() + template.file)

        print('doc template')
        print(doc)

        context = self.__get_contract_context(
            employee_doc=employee)
        doc.render(context)

        path_employee_dir = f"{os.getcwd()}/{frappe.local.site}/private/files/Employee/{employee.name}"
        frappe.create_folder(path_employee_dir)

        self._delete_old_contract(parent_folder_path=path_employee_dir)

        path = os.path.join(path_employee_dir, contract_code)
        timestamp = time.time()
        docx_path = f"{path}_{timestamp}.docx"
        print(f'docx_path = {docx_path}')

        doc.save(docx_path)

        pdf_path = self._generate_pdf(docx_path, path_employee_dir)

        # base_file_path = f"/public/files/{self.employee}"
        base_file_path = f"/private/files/Employee/{self.employee}"

        system_file_docx_path = f"{base_file_path}/{contract_code}_{timestamp}.docx"
        system_file_pdf_path = f"{base_file_path}/{contract_code}_{timestamp}.pdf"

        self._sync_to_file_manager(f"{contract_code}_{timestamp}.docx", system_file_docx_path,
                                   core.CoreConstants.DoctypeName.Contract, contract_code, f"Employee/{self.employee}", Path(docx_path).stat().st_size, is_private=True, is_force_new=False)

        self._sync_to_file_manager(file_name=f"{contract_code}_{timestamp}.pdf", file_url=system_file_pdf_path,
                                   dt=core.CoreConstants.DoctypeName.Contract, dn=contract_code, folder=f"Employee/{self.employee}", file_size=Path(pdf_path).stat().st_size, is_private=True, is_force_new=False)

        return system_file_docx_path, system_file_pdf_path

    def _update_contract_file(self):
        try:
            employee_doc: Employee = frappe.get_doc(core.CoreConstants.DoctypeName.Employee, self.employee)
            docx_path, pdf_path = self._generate_offer_letter(
                contract_code=self.name, employee=employee_doc)

            frappe.db.set_value(core.CoreConstants.DoctypeName.Contract, self.name, {
                "docx_file": docx_path,
                "pdf_file": pdf_path,
            })
            frappe.db.commit()

            self.reload()

        except Exception as e:
            print('cannot update contract file')
            raise e

    def on_update(self):
        print('on_update contract')
        self._update_contract_file()