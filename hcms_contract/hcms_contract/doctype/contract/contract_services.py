# Copyright (c) 2022, mas and contributors
# For license information, please see license.txt

import frappe
from hcms_contract.config.constant import *
from hcms.config.constant import *
import hcms_core.utils.constants as core


def _get_list_email_by_role(role : str) -> list:
	values = {
		"role": role,
	}
	print('role')
	print(role)
	list_email = frappe.db.sql(
		"""
			SELECT parent
			FROM `tabHas Role` AS tHR
			INNER JOIN (
				SELECT tU.name
				FROM tabEmployee tE
				INNER JOIN tabUser tU
				ON tE.work_email = tU.email
				WHERE tE.status = 'Active') AS tEmail
			ON tHR.parent = tEmail.name
			WHERE tHR.role = %(role)s
		""",
		values=values,
		as_dict = True,
	)
	print('_get_list_email_hr_director')
	print(list_email)
	return list_email

def _send_email_now(subject : str, message : str, recipients) -> None:
	frappe.sendmail(
		recipients=recipients,
		subject=subject,
		message=message,
		now=True,
	)


def _send_request_contract_approval(message : str):
	print('_notify_hiring_manager _get_list_email_hiring_manager')
	list_email = _get_list_email_by_role(role="HR Director")

	subject : str = 'Contract Approval Request'
	for email in list_email:
		_send_email_now(subject=subject, message=message, recipients=email['parent'])



def _get_list_transition():
    contract_workflow = frappe.get_doc(core.CoreConstants.DoctypeName.Workflow, 'Contract Workflow')
    return contract_workflow.transitions, contract_workflow.states


@frappe.whitelist()
def init_hr_responsibility():
	values = {
		'current_user_email': frappe.session.user
	}
	hr_incharge = frappe.db.sql(
		"""
			select name
			from `tabEmployee`
			where work_email = (select email
			from `tabUser`
			where name = %(current_user_email)s) and department = 'HRAD'
		""",
		values=values,
		as_dict=True
	)
	print(hr_incharge)
	if (len(hr_incharge) == 1):
		return hr_incharge[0]['name']

	return None


@frappe.whitelist() 
def change_workflow_state(contract : str, new_state):

	list_user_role = frappe.get_roles(frappe.session.user) 
	list_transition, list_state = _get_list_transition()
	is_new_state_valid = False

	contract_doc = frappe.get_doc(core.CoreConstants.DoctypeName.Contract, contract)
	hr_responsible = frappe.get_doc("Employee", contract_doc.hr_responsible)

	for transition in list_transition:
		if ((transition.allowed in list_user_role) and (contract_doc.workflow_state == transition.state) and (new_state == transition.next_state)):
			is_new_state_valid = True

		if is_new_state_valid:
			frappe.db.set_value(core.CoreConstants.DoctypeName.Contract, contract, "workflow_state", new_state)
			match (new_state):
				case ContractApprovalWorkflowState.Pending.value:
					_send_request_contract_approval(message=f"You have a request for offer letter approval of candidate {contract_doc.employee_name}.")
				case ContractApprovalWorkflowState.Approved.value:
					subject : str = 'Requested Contract Approved'
					message : str = f'Your requested contract for candidate {contract_doc.employee_name} has been approved'
					_send_email_now(subject=subject, message=message, recipients=hr_responsible.work_email)
				case ContractApprovalWorkflowState.Rejected.value:
					subject : str = 'Requested Contract Rejected'
					message : str = f'Your requested contract for candidate {contract_doc.employee_name} has been rejected'
					_send_email_now(subject=subject, message=message, recipients=hr_responsible.work_email)

			return True

	return False


def __change_workflow_state(new_state : str, contract : str):
	frappe.db.set_value(core.CoreConstants.DoctypeName.Contract, contract, "workflow_state", new_state)


def __cancel_contract(contract : str):
	frappe.db.set_value(core.CoreConstants.DoctypeName.Contract, contract, "enable", 0)
	frappe.db.set_value(core.CoreConstants.DoctypeName.Contract, contract, "is_primary", 0)
	frappe.db.set_value(core.CoreConstants.DoctypeName.Contract, contract, "status", ContractStatusMetadata.Cancelled.value)


@frappe.whitelist()
def handle_workflow_action(action : str, contract : str):
	try:
		list_user_role = frappe.get_roles(frappe.session.user) 
		list_transition, _ = _get_list_transition()
		is_action_valid = False

		contract_doc = frappe.get_doc(core.CoreConstants.DoctypeName.Contract, contract)
		hr_responsible = frappe.get_doc(core.CoreConstants.DoctypeName.Employee, contract_doc.hr_responsible)
		

		for transition in list_transition:
			if ((transition.allowed in list_user_role) and (contract_doc.workflow_state == transition.state) and (action == transition.action)):
				is_action_valid = True
				print('transition next state')
				print(transition.next_state)

			if is_action_valid:
				match (action):
					case WorkflowActionsMetadata.RequestForApproval.value:
						__change_workflow_state(new_state=transition.next_state, contract=contract)
						_send_request_contract_approval(message=f"You have a request for offer letter approval of candidate {contract_doc.employee_name}.")
					case WorkflowActionsMetadata.Approve.value:
						__change_workflow_state(new_state=transition.next_state, contract=contract)
						subject : str = 'Requested Contract Approved'
						message : str = f'Your requested contract for candidate {contract_doc.employee_name} has been approved'
						_send_email_now(subject=subject, message=message, recipients=hr_responsible.work_email)
					case WorkflowActionsMetadata.Reject.value:
						__cancel_contract(contract=contract)
						__change_workflow_state(new_state=transition.next_state, contract=contract)
						subject : str = 'Requested Contract Rejected'
						message : str = f'Your requested contract for candidate {contract_doc.employee_name} has been rejected'
						_send_email_now(subject=subject, message=message, recipients=hr_responsible.work_email)

				return True

		return False
	except BaseException as e:
		raise e
		# return False

