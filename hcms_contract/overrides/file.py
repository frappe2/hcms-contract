
import os
import frappe
from frappe import _
from frappe.core.doctype.file.file import File
from frappe.utils import get_files_path, get_site_path
from frappe.utils.file_manager import get_content_hash


class CustomFile(File):

    def __init__(self, *args, **kwargs):
        self.is_file_saved = False
        self.is_ignore_validate = False
        super().__init__(*args, **kwargs)

    def before_insert(self):
        self.set_folder_name()
        self.set_file_name()
        self.validate_attachment_limit()
        if self.is_folder:
            return
        if self.is_remote_file:
            self.validate_remote_file()
        else:
            # Custom script here, in some cases no need create file because other processes created.
            if not self.is_file_saved:
                self.save_file(content=self.get_content())
                self.flags.new_file = True
                frappe.local.rollback_observers.append(self)

    def validate(self):
        if not self.is_ignore_validate:
            super().validate()

    def generate_content_hash(self):
        if self.content_hash or not self.file_url or self.is_remote_file:
            return
        file_name = self.file_url.split("/")[-1]
        try:
            # Custom script here, in some cases no need create file because other processes created.
            if self.is_file_saved:
                file_path = f"{os.getcwd()}/{frappe.local.site}{self.file_url}"
            else:
                file_path = get_files_path(
                    file_name, is_private=self.is_private)
            with open(file_path, "rb") as f:
                self.content_hash = get_content_hash(f.read())
        except OSError:
            frappe.throw(_("File {0} does not exist").format(file_path))
