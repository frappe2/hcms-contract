const MAX_DURATION = 36

const PersonalDocumentTypeMetadata = {
	CCCD: "Citizen Identity Card/CCCD",
	CMND: "Identity Card/CMND",
	Passport: "Passport",
	DrivingLicense: "Driving License",
	Visa: "Visa",
	WorkPermit: "Work Permit",
	Other: "Other"
}

const ContractStatusMetadata = {
	New: "New",
	Running: "Running",
	Expired: "Expired",
	Cancelled: "Cancelled"
}

const ContractTypeMetadata = {
	Contract: "Contract",
	Decision: "Decision",
	Appendix: "Appendix",
	Agreement: "Agreement"
}

const ContractTermMetadata = {
	Internship: "Internship",
	Probation: "Probation",
	FixedTerm: "Fixed-term",
	Permanent: "Permanent"
}

const ContractApprovalStatus = {
	Pending: "Pending",
	Approved: "Approved",
	Rejected: "Rejected"
}

const WorkflowActionsMetadata = {
	RequestForApproval: "Request for approval",
	Approve: "Approve",
	Reject: "Reject"
}

const Messages = {
	Warning: "Warning",
	SaveFormFirst: 'Please save the form before taking any actions.',
	NotEnoughInfoProbation: "This employee does not have enough information to create a probation contract.",
	RequestForContractApproval: "Request for Contract Approval",
	ApproveContract: "Contract Approval",
	RejectContract: "Contract Rejection",
	CannotUndoneAction: 'This action cannot be undone',
	UnsavedTakeAction: 'Are you sure to save and ',
	SavedTakeAction: 'Are you sure to ',
	WaitingForApproval: 'Please wait for HR directors to approve probation contract before sending to candidate.'
}

const APIPath = {
    HandleWorkflowAction: "hcms_contract.hcms_contract.doctype.contract.contract_services.handle_workflow_action",
    InitHRResponsible: "hcms_contract.hcms_contract.doctype.contract.contract_services.init_hr_responsibility",
}

// execute: async function(frm, callback){
//     if (frm.is_dirty()){
//         let save_fn = async () => {
//             await frm.save()
//             callback()
//         }
//         // save_fn.resolve = () => callback()
//         save_fn().catch((err) => {
//             console.log(err)

//         })
//     }else{
//         callback()
//     }
// },