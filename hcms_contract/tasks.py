import frappe
from frappe.utils import getdate

def update_contract_status(today_date):
    list_contract = frappe.db.get_list(
        'Contract',
        filters={
            'enable': 1,
            'contract_status': ['IN', ['New', 'Running']],
        },
        fields = ['start_date', 'end_date', 'name'],
    )

    if (len(list_contract) > 0):
        for contract in list_contract:
            if ((contract['start_date'] <= today_date) and (today_date <= contract['end_date'])):
                frappe.db.set_value('Contract', contract['name'], 'contract_status', 'Running')
            elif (contract['end_date'] < today_date):
                frappe.db.set_value('Contract', contract['name'], 'contract_status', 'Expired')
                

def daily():
    today_date = getdate()
    update_contract_status(today_date)