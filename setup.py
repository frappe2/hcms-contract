from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in hcms_contract/__init__.py
from hcms_contract import __version__ as version

setup(
	name="hcms_contract",
	version=version,
	description="Contract App for HCMS",
	author="mas",
	author_email="thaihoang.pham@masterisehomes.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
